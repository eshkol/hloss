
## `hloss`

Hierarchy-aware loss implementation in `R`.

Functions are documented but code is not optimised (eventually I'll probably port some of this to C++).

All functions are documented.

```
library(hloss)

## Define cell populations
## If dataset-specific, also provide cell counts
nodes <- c(
  'Lymphocyte'                          = 10000,
  'Lymphocyte/B Cell'                   =  3000,
  'Lymphocyte/B Cell/Immature B Cell'   =  1000,
  'Lymphocyte/B Cell/Memory B Cell'     =  2000,
  'Lymphocyte/B Cell/Memory B Cell/IgM' =   600,
  'Lymphocyte/B Cell/Memory B Cell/IgA' =   650,
  'Lymphocyte/B Cell/Memory B Cell/IgG' =   750,
  'Lymphocyte/T Cell'                   =  7000,
  'Lymphocyte/T Cell/CD4+'              =  3000,
  'Lymphocyte/T Cell/CD8+'              =  4000
)

## If no cell counts available, define taxonomy by topology only, then calibrate automatically
tax <- TaxonomyFromPaths(
  name = 'Blood Cells',
  paths = names(nodes)
)
Calibrate(tax)

## If cell counts available, use them in calibration
tax <- CalibratedTaxonomy(
  name = 'Blood Cells',
  paths = names(nodes),
  counts = nodes
)

plot(tax)

```

Coming soon: accuracy, pseudo-ARI and F1 scoring which takes these penalties into account.
